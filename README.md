### **简介** 

Discuz! X 官方 Git/SVN (https://git.oschina.net/ComsenzDiscuz/DiscuzX) ，简体中文 UTF8 版本，其他版本请自行转码或者在 Discuz! 官方站下载安装包。

### **声明**
您可以 Fork 本站代码，但未经许可 **禁止** 在本产品的整体或任何部分基础上以发展任何派生版本、修改版本或第三方版本用于 **重新分发** 

### **相关网站**
 
- Discuz! 官方站：http://www.discuz.net
- Discuz! 应用中心：http://addon.discuz.com
- Discuz! 开放平台：http://open.discuz.net

### **感谢 Fans**

- DiscuzFans：https://git.oschina.net/sinlody/DiscuzFans
- DiscuzLite：https://git.oschina.net/3dming/DiscuzL

### **DxGit Forker 交流群**
[QQ群 62652633](http://shang.qq.com/wpa/qunwpa?idkey=5c7c9ff98ebd001751bcda84b47c77830c554f729c72c247957cd86bdd83aa47) 本群仅限 Fork 本 Git 的用户加入，入群请提供您 Fork 的地址（不知道啥是Fork？[看这里](http://git.mydoc.io/?t=180700))

### **友情提示**
从2017-07-08起已进入X3.4功能范畴，X3.3已发布Release，并不再更新。X3.4版本主旨在清除云平台相关功能，由于涉及删除文件，非正常方式的更新和升级将导致运行错误，请确保您真需要升级到此版本。升级前请严格遵循升级规范，建立文件夹 old，旧程序除了 data、config、uc_client、uc_server 目录以外的程序移动进入 old 目录中后再更新新版文件。